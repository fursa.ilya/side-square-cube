package org.studwork.square;

public abstract class Figure {

    //Находим площадь квадрата, этот метод пригодится
    // в обоих классах, как и для квадрата, так и для куба
    public double findSquare(Side s) {
        double side = s.getValue();
        return Math.pow(side, 2);
    }
}
