package org.studwork.square;

public class Cube extends Figure {
    private double thickness; //Толщина стенки куба
    private Side s; //ребро куба

    public Cube(double thickness, Side s) {
        this.thickness = thickness;
        this.s = s;
    }

    public double volume() {
        Side newSide = new Side(s.getValue() - thickness); //Учитываем толщину стенки
        double surface = findSquare(newSide); //Площадь поверхности
        return Math.pow(surface, 2); //Возводим третий раз в квадрат
    }

    public double square() {
        return 6 * findSquare(s); //Находим площадь куба для сравнения какой из кубов больше
    }
}
