package org.studwork.square;

/**
 * Класс описывающий сторону
 * содержит значене в double
 */
public class Side {
    private double value;

    public Side(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
