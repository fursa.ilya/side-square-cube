package org.studwork.square;

public class Square extends Figure {
    private Side s; //Ребро

    public Square(Side s) {
        this.s = s;
    }

    public double square() {
        return findSquare(s); //вызываем метод из абстрактного класса
    }
}
