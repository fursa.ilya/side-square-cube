package org.studwork.square;

public class Solution {

    public static void main(String[] args) {
        Side s1 = new Side(2.0); //ребро
        double th1 = 3.0;
        Side s2 = new Side(3.0);
        double th2 = 2.5; //th - толщина стенки
        Cube c1 = new Cube(th1, s1); //Создаем первый куб
        Cube c2 = new Cube(th2, s2); //Создаем второй куб

        double c1Square = c1.square();
        double c2Square = c2.square();

        double c1Volume = c1.volume();
        double c2Volume = c2.volume();

        System.out.println("Площадь 1 куба: " + c1Square);
        System.out.println("Площадь 2 куба: " + c2Square);

        System.out.println("Объем 1 куба: " + c1Volume);
        System.out.println("Объем 2 куба: " + c2Volume);

        if(c1Square > c2Square) { //Сравниваем по площади
            System.out.println("1 куб больше 2 по площади");
        } else {
            System.out.println("2 куб больше 1 по площади");
        }
        //Сравниваем по объему
        if(c1Volume > c2Volume) {
            System.out.println("1 куб объемнее 2. Потребуется " + (c1Volume / c2Volume) + " кубов объемом как у 2 чтобы заполнить 1");
        } else {
            System.out.println("2 куб объемнее 1. Потребуется " + (c2Volume / c1Volume) + " кубов объемом как у 1 чтобы заполнить 2");

        }

    }

}
